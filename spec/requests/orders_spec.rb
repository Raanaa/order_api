require 'rails_helper'

RSpec.describe "Orders", type: :request do
 
# initialize test data 
  let!(:orders) { create_list(:order, 10) }
  let(:order_id) { orders.first.id }
  let(:user) { FactoryBot.create(:user, first_name: 'user1', middle_name: 'middle_1' , last_name: 'last_1' , email: 'rana39993@live.com' , phone_number: 'phone', password: 'password' ) }

  # Test POST /orders
  describe 'POST /orders' do
    let(:valid_attributes) { { total_price: 100} }

    context 'when the request is valid' do
      before { post '/api/v1/orders', params: valid_attributes , headers: { 'Authorization' => AuthenticationTokenService.call(user.id) }}

      it 'creates an order' do
        expect(json['total_price']).to eq(100)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/orders', params: { },headers: { 'Authorization' => AuthenticationTokenService.call(user.id) } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match("{\"total_price\":[\"can't be blank\"]}")
      end
    end
  end

  # Test PUT /orders/:id
  describe 'PUT /orders/:id' do
    let(:valid_attributes) { { status: 'accepted' } }

    context 'when the record exists' do
      before { put "/api/v1/orders/#{order_id}", params: valid_attributes , headers: { 'Authorization' => AuthenticationTokenService.call(user.id) }}

      it 'updates the record' do
        expect(json).not_to be_empty
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
end