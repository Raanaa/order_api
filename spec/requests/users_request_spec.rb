require 'rails_helper'
RSpec.describe 'Users', type: :request do
    describe 'POST /register' do
      it 'authenticates the user' do
        post '/api/v1/register', params: { user: { first_name: 'user1', middle_name: 'middle_1' , last_name: 'last_1' , email: 'rana39993@live.com' , phone_number: 'phone', password: 'password'  } }
        expect(response).to have_http_status(:created)
        expect(json).to eq({
                             'id' => User.last.id,
                             'first_name' => 'user1',
                             'middle_name' => 'middle_1',
                             'last_name' => 'last_1',
                             'email' => 'rana39993@live.com',
                             'phone_number' => 'phone',
                             'token' => AuthenticationTokenService.call(User.last.id)
                           })
      end
    end




  # Test GET /users/user_order
  describe 'GET /users/user_order' do
    let(:user) { FactoryBot.create(:user, first_name: 'user1', middle_name: 'middle_1' , last_name: 'last_1' , email: 'rana39993@live.com' , phone_number: 'phone', password: 'password' ) }

    before { get "/api/v1/users/user_order", headers: { 'Authorization' => AuthenticationTokenService.call(user.id) } }

    context 'when the record exists' do
      it 'returns the record' do
        expect(json)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns a not found message' do
        expect(json).to be_empty
      end
    end
  end


end