RSpec.describe 'Authentications', type: :request do
    describe 'POST /login' do
      let(:user) { FactoryBot.create(:user, first_name: 'user1', middle_name: 'middle_1' , last_name: 'last_1' , email: 'rana39993@live.com' , phone_number: 'phone', password: 'password' ) }
      it 'authenticates the user' do
        post '/api/v1/login', params: { first_name: user.first_name, middle_name: user.middle_name , last_name: user.last_name , email: user.email , phone_number: user.phone_number , password: 'password' }
        expect(response).to have_http_status(:created)
        expect(json).to eq({
                            'id' => user.id,
                            'first_name' => 'user1',
                            'middle_name' => 'middle_1',
                            'last_name' => 'last_1',
                            'email' => 'rana39993@live.com',
                            'phone_number' => 'phone',
                            'token' => AuthenticationTokenService.call(User.last.id)
                           })
      end

      it 'returns error when email does not exist' do
        post '/api/v1/login', params: { email: "new_email"  , password: 'password' }
        expect(response).to have_http_status(:unauthorized)
        expect(json).to eq({
                             'error' => 'No such user'
                           })
      end
      it 'returns error when password is incorrect' do
        post '/api/v1/login', params: { email: user.email  , password: 'incorrect_password' }
        expect(response).to have_http_status(:unauthorized)
        expect(json).to eq({
                             'error' => 'Incorrect password '
                           })
      end
    end
end