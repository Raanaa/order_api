require 'rails_helper'

RSpec.describe Order, type: :model do

   it { should validate_presence_of(:total_price) }

   describe 'Associations' do
      it { should have_many(:user_orders) }
      it { should have_one(:order_detail) }
      it { should have_many(:users).through(:user_orders) }
    end

end