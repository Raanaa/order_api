FactoryBot.define do
    factory :order do
        total_price { Faker::Number.number(digits: 10) }
        status { "pending" }
    end
end