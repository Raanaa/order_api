class UpdateUserOrderTable < ActiveRecord::Migration[6.1]
  def change
    add_reference :user_orders, :user, foreign_key: true
    add_reference :user_orders, :order, foreign_key: true
  end
end
