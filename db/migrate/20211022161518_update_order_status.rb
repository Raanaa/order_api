class UpdateOrderStatus < ActiveRecord::Migration[6.1]
  def change
    change_column :orders, :status, :integer, default: "pending"
  end
end
